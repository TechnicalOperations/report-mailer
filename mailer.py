import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.base import MIMEBase
from email import encoders
from . import parse
import json


class Mailer:

    def __init__(self, authentication, template, default='HTML can not be rendered', subs={}):
        self.parse = parse.TemplateParse()
        with open(authentication, 'r') as f:
            self.auth = json.loads(f.read())
        self.default = default
        self.template = template
        self.graphs = {}
        self.colors = []
        self.files = []
        self.formatter = {}
        self.cids = None
        self.subs = subs

    def set_graph_colors(self, colors):
        self.colors = colors

    def set_formatter(self, formatter={}):
        self.formatter = formatter

    def make_html(self):
        self.template = self.parse.replace_tags(self.template).format(**self.formatter)

    def make_graph(self, cid, data, *args, **kwargs):
        if self.cids is None:
            with open(self.template, 'r') as f:
                self.template = f.read()
            for k, v in self.subs.items():
                self.template = self.template.replace('{'+k.replace(' ', '')+'}', v)
            self.cids = self.parse.get_cids(self.template)
        meta = self.cids.get(cid, None)
        if meta is None:
            raise Exception("CID '{0}' does not exist in template".format(cid))
        method = self.parse.methods[meta['kind']]
        meta['title'] = kwargs.pop('title') if 'title' in kwargs else meta['title']
        colors = kwargs.pop('colors') if 'colors' in kwargs else None
        colors = colors if colors is not None else self.colors if len(self.colors) > 0 else parse.TemplateParse.DEFAULT_COLORS
        if 'multi' in meta['kind']:
            self.graphs[cid] = method(
                data,
                args[0],
                meta['title'] if len(args) < 2 else args[1],
                colors=colors,
                **kwargs
            )
        else:
            self.graphs[cid] = method(
                data,
                None,
                meta['title'],
                colors=colors,
                **kwargs
            )
        return zip(list(data[list(data.columns)[0]]), colors)

    def attach_file(self, file):
        with open(file, 'rb') as f:
            data = f.read()
        attch = MIMEBase('application', 'octet-stream')
        attch.set_payload(data)
        encoders.encode_base64(attch)
        attch.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(file))
        self.files.append(attch)
        return attch

    def send_mail(self, subject, to, cc=[], bcc=[], **kwargs):
        self.make_html()
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = self.auth['email']
        msg['To'] = to
        msg['CC'] = ', '.join(cc)

        msg.attach(MIMEText(self.default, 'plain'))
        msg.attach(MIMEText(self.template, 'html'))

        for cid, graph in self.graphs.items():
            img = MIMEImage(graph, 'png')
            img.add_header('Content-Disposition', 'attachement; filename={0}.png'.format(cid))
            img.add_header('Content-ID', '<{0}>'.format(cid))
            msg.attach(img)

        for file in self.files:
            msg.attach(file)

        s = smtplib.SMTP()
        s.connect(**self.auth['con'])
        s.ehlo()
        s.starttls()
        s.ehlo()
        s.login(self.auth['email'], self.auth['password'])
        s.sendmail(self.auth['email'],
                   cc+bcc+[to],
                   msg.as_string())
