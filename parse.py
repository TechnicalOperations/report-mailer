import os
import re
import locale
import matplotlib
matplotlib.use('Agg')
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.ticker import FuncFormatter
from io import BytesIO


locale.setlocale(locale.LC_ALL, 'en_US')


def contract(x, pos):
    inv = 0
    while x * (10 ** inv) > 1:
        inv -= 1
    inv += 1
    if inv <= -6:
        return '%0.1fM' % (x / 1000000)
    elif inv <= -3:
        return '%0.1fK' % (x / 1000)
    else:
        return '%0.1f' % x


class TemplateParse:

    CHART_TYPES = {
        'pie': None,
        'line': None,
        'bar': None
    }

    DEFAULT_COLORS = ['#48d1f0', '#e780ac', '#8be8f8', '#a780c7', '#80d5c2', '#fcee80', '#bebdb7']

    def __init__(self):
        self.cids_reg = re.compile('\{\{.+?\}\}')
        self.methods = {
            'pie': self.make_pie,
            'bar': self.make_bar,
            'line': self.make_line,
            'multiline': self.multi_axes_line
        }

    def replace_tags(self, template):
        cids = self.get_cids(template)
        for cid, meta in cids.items():
            template = template.replace(meta['tag'], 'cid:{0}'.format(cid))
        return template

    def get_cids(self, template):
        tags = self.cids_reg.findall(template)
        tags = [(i, i.replace('{', '').replace('}', '').strip().split(':'),) for i in tags]
        tags = {
            i[1][1]: {
                'kind': i[1][0],
                'title': i[1][1],
                'legend': i[1][2] if len(i[1]) > 2 else '',
                'tag': i[0]
            }
            for i in tags
        }
        return tags

    def make_image(self):
        buffer = BytesIO()
        plt.savefig(buffer, format='png')
        buffer.seek(0)
        data = buffer.read()
        plt.close()
        return data

    def make_pie(
            self,
            data,
            legend,
            title,
            colors=DEFAULT_COLORS,
            autopct=lambda x: '{p:.2f}%'.format(p=x)
    ):
        fig, ax = plt.subplots()
        ax.pie(data[list(data.columns)[1]], autopct=autopct, colors=colors)
        if title is not None:
            ax.set_title(title)
        if legend is not None:
            ax.legend(legend)
        return self.make_image()

    def make_bar(self,
                 data,
                 legend,
                 title,
                 top_labels=True,
                 colors=DEFAULT_COLORS,
                 width=0.5,
                 fontsize=12,
                 rot=None):
        l = len(list(data.columns)[1:])
        width = width / 2
        keys = list(data.columns)[1:]
        m = max([max(data[col]) for col in keys])
        shift = 0
        ax = data.plot(x=list(data.columns)[0], y=list(data.columns[1:]), kind='bar', color=colors[:l], fontsize=fontsize)
        if top_labels:
            for key in keys:
                for i, v in enumerate(data[key]):
                    ax.text(
                        i - width + (shift * width),
                        v + m * 0.01,
                        locale.format("%d", int(np.round(v, 0)), grouping=True),
                        color='black',
                        fontsize='x-small'
                    )
                shift += 1
        ax.grid('on', axis='y', alpha=0.4)
        if title is not None:
            ax.set_title(title)
        ax.set_xlabel('')
        plt.xticks(plt.xticks()[0], plt.xticks()[1], rotation=rot)
        plt.yticks(list(plt.yticks()[0])+[max(plt.yticks()[0]) * 1])
        if legend is not None:
            ax.legend(legend)
        else:
            ax.legend(list(data.columns)[1:])
        return self.make_image()

    def make_line(self,
                  data,
                  legend,
                  title,
                  colors=DEFAULT_COLORS,
                  y_lim=None,
                  suffix=True):
        l = len(list(data.columns)[1:])
        ax = data.plot(x=list(data.columns)[0], y=list(data.columns)[1:], kind='line', color=colors[:l])
        ax.grid('on', axis='y', alpha=0.4)
        if title is not None:
            ax.set_title(title)
        ax.set_xlabel('')
        plt.xticks(plt.xticks()[0], plt.xticks()[1], rotation=0)
        plt.yticks(list(plt.yticks()[0])+[max(plt.yticks()[0]) * 1])
        if y_lim is not None:
            ax.set_ylim(y_lim)
        if legend is not None:
            ax.legend(legend)
        else:
            ax.legend(list(data.columns)[1:])
        if suffix:
            formatter = FuncFormatter(contract)
            ax.yaxis.set_major_formatter(formatter)
        return self.make_image()

    def multi_axes_line(self,
                        left,
                        right,
                        title,
                        colors=DEFAULT_COLORS,
                        shade_right=True,
                        left_y_lim=None,
                        right_y_lim=None,
                        suffix=True):
        ax = left.plot(x=list(left.columns)[0], y=list(left.columns)[1:], legend=False, color=colors[1:len(left.columns)-1])
        ax2 = ax.twinx()
        ax2 = right.plot(x=list(right.columns)[0],
                         y=list(right.columns)[1:],
                         ax=ax2,
                         legend=False,
                         color=colors[len(left.columns):])
        if shade_right:
            ax2.fill_between(list(right[list(right.columns)[0]]), 0, list(right[list(right.columns)[1]]), alpha=0.1)
        if left_y_lim is not None:
            ax.set_ylim(left_y_lim)
        if right_y_lim is not None:
            ax2.set_ylim(right_y_lim)
        ax.legend(list(left.columns)[1:], loc='upper left')
        ax2.legend(list(right.columns)[1:], loc='upper right')
        if suffix:
            formatter = FuncFormatter(contract)
            ax.yaxis.set_major_formatter(formatter)
            ax2.yaxis.set_major_formatter(formatter)
        plt.title(title)
        return self.make_image()

