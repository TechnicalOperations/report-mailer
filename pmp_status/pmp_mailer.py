import pandas
import locale
import MySQLdb
import numpy as np
from tools import sql
from report_mailer import mailer
from datetime import datetime, timedelta
pandas.set_option('display.width', 5000)
locale.setlocale(locale.LC_ALL, 'en_US')


con = MySQLdb.connect(read_default_file='/var/auth/.my.conf', db='dashboard')
date = datetime.now()
date = date - timedelta(hours=date.hour, days=4)
q = """
	SELECT *
	FROM index_pmpperformance
	WHERE date >= STR_TO_DATE('{start}', '%Y-%m-%d')
""".format(start=date.strftime('%Y-%m-%d'))
data = pandas.read_sql_query(q, con)
con.close()
data = data.groupby(['date', 'deal_id', 'ssp_id', 'dsp_id']).sum().reset_index()

q = """SELECT external_deal_id AS deal_id, description FROM ods.rx.deal GROUP BY external_deal_id, description"""
con = sql.connect()
details = pandas.read_sql_query(q, con)
sql.close()
details = details.rename(columns={i: i.lower() for i in details.columns}).to_dict(orient='records')
details = {i['deal_id']: i['description'] for i in details}

deals = data[(data['date'] == data['date'].max())][['deal_id', 'margin', 'revenue']].groupby('deal_id').sum().reset_index()
deals = deals.sort_values(by='revenue', ascending=False).reset_index(drop=True)
top = set(deals['deal_id'][:6])
deals['deal_id'] = deals['deal_id'].apply(lambda x: details.get(x, 'Managed - No Deal') if x in top else 'Other')
deals = deals.groupby('deal_id').sum().sort_values(by='revenue', ascending=False).reset_index()
top_margin = deals[['deal_id', 'margin']]
top_revenue = deals[['deal_id', 'revenue']]
total = data[['date', 'revenue', 'margin']].groupby('date').sum().reset_index()


m = mailer.Mailer('/var/auth/.em', 'template.html')
m.make_graph('top_revenue', top_revenue, title='Revenue')
legend = m.make_graph('top_margin', top_margin, title='Margin')
m.make_graph('total', total, colors=['#16d1f0', '#fedd00'], title=None)

top_legend = "\n".join([
    """<tr><td style="background-color: {1}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>{0}</td></tr>""".format(i[0], i[1])
    for i in legend
])

total_revenue = int(np.round(data[(data['date'] == data['date'].max())]['revenue'].sum(), 0))
total_margin = int(np.round(data[(data['date'] == data['date'].max())]['margin'].sum(), 0))
current_date = data['date'].max().strftime('%m/%d/%Y')
m.set_formatter(formatter={
    'total_revenue': locale.format("%d", total_revenue, grouping=True),
    'total_margin': locale.format("%d", total_margin, grouping=True),
    'date': current_date,
    'top_legend': top_legend
})

m.send_mail(
    "Pmp Status for {0} - ${1}".format(current_date, total_revenue),
    "cbeecher@rhythmone.com",
    cc=["mvillaseca@rhythmone.com"])
