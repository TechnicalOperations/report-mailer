SELECT REGEXP_SUBSTR(request, '/rmp/([^/]+)/0/v(o|p)\\?z=1r', 1, 1, 'e') AS ssp,
    publisher,
    adspot AS placement
FROM raw_rmp.public.raw_data_big
WHERE time >= '2017-09-20'
    AND REGEXP_SUBSTR(request, '/rmp/([^/]+)/0/v(o|p)\\?z=1r', 1, 1, 'e') IS NOT NULL
GROUP BY request, publisher, placement;
