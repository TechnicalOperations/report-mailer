import os
import pandas
import locale
import matplotlib
matplotlib.use('Agg')
import numpy as np
from tools import sql
from report_mailer import mailer
from datetime import datetime, timedelta
pandas.set_option('display.width', 5000)
locale.setlocale(locale.LC_ALL, 'en_US')

days = 7
start = datetime.now() - timedelta(days=days)
start = datetime(start.year, start.month, start.day)
end = start + timedelta(days=days)


publishers = pandas.read_csv('publishers.csv')
publishers['publisher'] = publishers['publisher'].astype(str)
publishers['placement'] = publishers['placement'].astype(str)
q = """
    WITH wins AS (
        SELECT event_date, pub_id, media_type, SUM(impression_pixel) AS imps, SUM(revenue) AS revenue
        FROM r1_edw.rx.impression_d_est
        WHERE event_date >= '{start}'
            AND event_date < '{end}'
            AND ssp_id = 'rmpssp'
            AND (pub_id IN ('{pubs}')
                OR site_id IN ('{placs}'))
        GROUP BY event_date, pub_id, media_type
    ),
    responses AS (
        WITH items AS (
            SELECT DATE_TRUNC('DAY', A.event_time_est) AS event_date,
                A.request_id,
                A.pub_id,
                A.media_type,
                MAX(B.sample_rate) AS responses
            FROM rx.bidrequest AS A
            INNER JOIN rx.bidresponse AS B ON A.request_id=B.request_id
            WHERE A.event_time_est >= '{start}'
                AND A.event_time_est < '{end}'
                AND B.event_time_est >= '{start}'
                AND B.event_time_est < '{end}'
                AND A.ssp_id = 'rmpssp'
                AND (A.pub_id IN ('{pubs}')
                    OR A.site_id IN ('{placs}'))
            GROUP BY event_date, A.request_id, A.pub_id, A.media_type
        )
        SELECT event_date, pub_id, media_type, SUM(responses) AS responses
        FROM items
        GROUP BY event_date, pub_id, media_type
        
    ),
    matches AS (
        SELECT DATE_TRUNC('DAY', event_time_est) AS event_date,
            pub_id,
            media_type,
            SUM(IFF(
                media_type = 'site' AND ssp_request:user:buyeruid::string LIKE 'RX-%',
                sample_rate,
                IFF(ssp_request:device:ifa::string IS NOT NULL
                    OR ssp_request:device:didsha1::string IS NOT NULL
                    OR ssp_request:device:didmd5::string IS NOT NULL
                    OR ssp_request:device:dpidsha1::string IS NOT NULL
                    OR ssp_request:device:dpidmd5::string IS NOT NULL
                    OR ssp_request:device:macsha1::string IS NOT NULL
                    OR ssp_request:device:macmd5::string IS NOT NULL,
                    sample_rate,
                    0)
            )) AS user_matches,
            SUM(sample_rate) AS requests
        FROM rx.bidrequest
        WHERE event_time_est >= '{start}'
            AND event_time_est < '{end}'
            AND ssp_id = 'rmpssp'
            AND (pub_id IN ('{pubs}')
                OR site_id IN ('{placs}'))
        GROUP BY event_date, pub_id, media_type
    )
    SELECT B.event_date AS date, B.pub_id AS publisher, A.media_type,
        B.user_matches AS user_matches,
        B.requests, C.responses, A.imps AS impression_pixels, A.revenue AS revenue
    FROM matches AS B
    LEFT JOIN wins AS A ON A.event_date=B.event_date
                        AND A.pub_id=B.pub_id
                        AND A.media_type=B.media_type
    LEFT JOIN responses AS C ON C.event_date=B.event_date
                        AND C.pub_id=B.pub_id
                        AND C.media_type=B.media_type
""".format(start=start.strftime('%Y-%m-%d'), pubs="', '".join(publishers['publisher']), placs="', '".join(publishers['placement']), end=end)
con = sql.connect()
data = pandas.read_sql_query(q, con)
sql.close()
data = data.rename(columns={i: i.lower() for i in data.columns})
data['requests'] = data['requests'].astype(int)
pubs = publishers[['publisher', 'publisher_name']].to_dict(orient='records')
pubs = {i['publisher']: i['publisher_name'] for i in pubs}
# placs = publishers[['placement', 'placement_name']].to_dict(orient='records')
# placs = {i['placement']: i['placement_name'] for i in placs}
ssps = publishers[['publisher', 'ssp']].to_dict(orient='records')
ssps = {i['publisher']: i['ssp'] for i in ssps}
data['ssp'] = data['publisher'].apply(lambda x: ssps.get(x, 'other'))
data['publisher'] = data['publisher'].apply(lambda x: pubs.get(x, ''))
# data['placement'] = data['placement'].apply(lambda x: placs.get(x, ''))
data = data[['date', 'ssp', 'publisher', 'user_matches', 'media_type', 'requests', 'responses', 'impression_pixels', 'revenue']]
data['media_type'] = data['media_type'].fillna('unknown')
data = data.fillna(0)

matches = data[['ssp', 'media_type', 'user_matches', 'requests']].copy()
matches = pandas.pivot_table(matches, columns=['media_type'], index=['ssp'], fill_value=0, aggfunc=np.sum)
# matches.columns = matches.columns.swaplevel(0, 1)
matches = matches['user_matches'] * 100.0 / matches['requests']
matches = matches.reset_index()
matches.columns.name = None
matches = matches.reset_index()[['ssp', 'site', 'app']].fillna(0)

pubs = list([(k, v,) for k, v in pubs.items() if v in set(data['publisher'])])
pubs.sort(key=lambda x: (
    0 - data[(data['ssp'] == data[(data['publisher'] == x[1])].iloc[0]['ssp'])]['responses'].sum(),
    0 - data[(data['publisher'] == x[1])]['responses'].sum(),)
)

templates = []
ssps = list(set(data['ssp']))
for ssp in ssps:
    tmp_pubs = [i for i in pubs if i[1] in set(data[(data['ssp'] == ssp)]['publisher'])]
    template = '<h2>{ssp}</h2><table>'.format(ssp=ssp[0].upper()+ssp[1:].lower()) + ''.join([
        "<tr>" + ''.join(['<td><img src="{{ multiline:pub_{0} }}"</td>'.format(z[0]) for z in tmp_pubs[i: i+3]]) + "</tr>"
        for i in range(0, len(tmp_pubs), 3)
    ]) + '</table>'
    template = template.replace('{', '{{').replace('}', '}}')
    templates.append(template)
template = '\n'.join(templates)

subs = {
    'pubs': template
}
m = mailer.Mailer(os.path.expanduser('~')+'/.auth/.em_json', 'template.html', subs=subs)
m.make_graph('user_matches', matches, colors=['#16d1f0', '#fedd00'], title="User Match Rates")
m.make_graph(
    'ssp_revenue',
    data[['ssp', 'revenue']].groupby('ssp').sum().reset_index(),
    title='Revenue By SSP'
)

matches = data[['date', 'media_type', 'user_matches', 'requests']].groupby(['date', 'media_type']).sum().reset_index()
matches['match_rate'] = matches['user_matches'] * 100.0 / matches['requests']
matches = matches[['date', 'media_type', 'match_rate']]
matches = pandas.pivot_table(matches, columns=['media_type'], index=['date'], fill_value=0, aggfunc=np.sum)
matches.columns = matches.columns.droplevel(0)
matches.columns.name = None
matches = matches.reset_index()[['date', 'site', 'app']]
m.make_graph('user_matches_4', matches, colors=['#16d1f0', '#fedd00'], title="Total Trend (7 Days)")

matches = data[['publisher', 'user_matches', 'requests']].groupby('publisher').sum()
matches = matches.sort_values(by='requests', ascending=False)[:5]
matches = matches.reset_index()
matches['User Match Rate'] = matches['user_matches'] * 100.0 / matches['requests']
matches = matches[['publisher', 'User Match Rate']]
m.make_graph('user_matches_pub', matches, colors=['#16d1f0', '#fedd00'], title="Match Rate of Top 5 Publishers", fontsize=6, rot=-8)

matches = data[data['publisher'].isin(matches['publisher'])][['date', 'publisher', 'user_matches', 'requests']].groupby(['date', 'publisher']).sum()
matches = matches.reset_index()
matches['match_rate'] = matches['user_matches'] * 100.0 / matches['requests']
matches = matches[['date', 'publisher', 'match_rate']]
matches = pandas.pivot_table(matches, index=['date'], columns=['publisher'], fill_value=0, aggfunc=np.sum)
matches.columns = matches.columns.droplevel(0)
matches = matches.reset_index()
matches.columns.name = None
m.make_graph('user_matches_4_pub', matches, title="Match Rate of Top 5 Publishers (7 Days)")

perf = data[['publisher', 'requests', 'impression_pixels']]
perf = perf.groupby(['publisher']).sum()
perf['imp_rate'] = perf['impression_pixels'] * 100.0 / perf['requests']
del perf['impression_pixels']
del perf['requests']
perf = perf.reset_index()
perf = perf.rename(columns={'imp_rate': 'Efficiency Rate'})
perf = perf.sort_values(by='Efficiency Rate', ascending=True)
m.make_graph('low_perf', perf[:4], title="Lowest Performing Publishers", fontsize=6, rot=-8)
perf = perf.sort_values(by='Efficiency Rate', ascending=False)
m.make_graph('high_perf', perf[:4], title="Highest Performing Publishers", fontsize=6, rot=-8)

perf = data[['publisher', 'responses', 'impression_pixels', 'revenue']]
perf = perf.groupby(['publisher']).sum()
perf['imp_rate'] = perf['impression_pixels'] * 100.0 / perf['responses']
del perf['impression_pixels']
del perf['responses']
perf = perf.reset_index()
perf = perf.rename(columns={'imp_rate': 'Fill Rate'})
perf = perf.sort_values(by='Fill Rate', ascending=True)
m.make_graph('low_perf_res', perf[:4], title="Lowest Performing Publishers", fontsize=6, rot=-8)
perf = perf.sort_values(by='Fill Rate', ascending=False)
m.make_graph('high_perf_res', perf[:4], title="Highest Performing Publishers", fontsize=6, rot=-8)

pdata = data[['date', 'publisher', 'requests', 'responses', 'impression_pixels', 'revenue']]
pdata = pdata.groupby(['date', 'publisher']).sum().reset_index()
res_lim = (0, pdata['responses'].max(),)
req_lim = (0, pdata['requests'].max(),)
pdata = pandas.pivot_table(pdata, index=['date'], columns=['publisher'], fill_value=0, aggfunc=np.sum)
pdata.columns = pdata.columns.swaplevel(0, 1)

for pub in pubs:
    name = pub[1]
    ptmp = pdata[pub[1]].copy().reset_index()
    pub = pub[0]
    m.make_graph('pub_{0}'.format(pub),
                 ptmp[['date', 'responses', 'impression_pixels']],
                 ptmp[['date', 'requests']],
                 "{0} - {1} - ${2}".format(
                     data[(data['publisher'] == name)].iloc[0]['ssp'],
                     name,
                     ptmp['revenue'].sum()
                 ),
                 shade_right=True,
                 left_y_lim=res_lim,
                 right_y_lim=req_lim)

current_date = data['date'].max().strftime('%m/%d/%Y')
f = 'RMP_Server2Server_Stats_{0}.xlsx'.format(current_date.replace('/', '_'))
data.to_excel(f, index=False)
m.attach_file(f)
os.remove(f)
m.set_formatter(formatter={
    'date': current_date
})

m.send_mail(
    "S2S Status for Past 7 days",
    "cbeecher@rhythmone.com",
    cc=["mvillaseca@rhythmone.com"]
)
